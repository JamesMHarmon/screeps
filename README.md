# Screeps

## Getting Started

### Requirements

* [Node.js](https://nodejs.org/en/) (v4.0.0+)
* Gulp 4.0+ - `sudo npm install -g gulpjs/gulp.git#4.0`

### Quick setup

First, copy the `example.config.json` to `config.json` and change the `username` and `password` properties with your Screeps credentials.

The `config.json` file is where you set up your development environment. If you want to push your code to another branch, for example, if you have some sort of a staging branch where you test around in Simulation mode, we have left a `branch` option for you to easily change the target branch of the upload process. The `default` branch is set as the default.

The `src/config/config.ts` file is where you store your code-specific config variables. For example, if you want to easily turn on `PathFinder` when needed, you can set your own variable here. Once you've set up your configs, import the `config.ts` file on the file you want to call these configs at:

```js
import * as Config from "../path/to/config";
```

Then simply call the config variables with `Config.CONFIG_VARIABLE`.

**WARNING: DO NOT** commit these files into your repository!

### Installing npm modules

Then run the following the command to install the required npm packages and TypeScript type definitions.

```bash
$ npm install
```

### Running the compiler

```bash
# To compile your TypeScript files on the fly
$ npm start

# To deploy the code to Screeps
$ npm run deploy
```

You can also use `deploy-prod` instead of `deploy` for a bundled version of the project, which has better performance but is harder to debug.

`deploy-local` will copy files into a local folder to be picked up by steam client and used with the official or a private server.

## Notes

A `main.ts` file exists with a `loop()` function. This is necessary in order to run the game loop.

**Source:** http://support.screeps.com/hc/en-us/articles/204825672-New-main-loop-architecture

### Source maps

Works out of the box with "npm run deploy-prod" and default values from src/config/config.example.ts. Links back to source control when possible (currently understands only github and gitlab). Code has to be committed at build time and pushed to remote at run time for this to work correctly.

Doesn't work in sim, because they do lots of evals with scripts in sim.

Currently maps are generated, but "source-maps" module doesn't get uploaded for non-webpack builds.

Log level and output can be controlled from console by setting level, showSource and showTick properties on log object.

```js
// print errors only, hide ticks and source locations
log.level = 1;
log.showSource = false;
log.showTick = false;
```

## Thanks to

[screeps-typescript-starter](https://github.com/screepers/screeps-typescript-starter)
