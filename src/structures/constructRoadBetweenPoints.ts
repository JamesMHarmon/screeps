function constructRoadBetweenPoints(start: RoomPosition, end: RoomPosition): undefined {
	// Set the cost of plains and swamps to be equal since roads will be built ontop of them.
	let path = PathFinder.search(start, end, {
		maxOps: 10000,
		maxRooms: 1,
		roomCallback: (roomName: string) => {
			let room = Game.rooms[roomName];
			let costMatrix = new PathFinder.CostMatrix();

			(room.lookAtArea(0, 0, 49, 49, true) as LookAtResultWithPos[]).forEach((loc => {
				if (
					loc.constructionSite && loc.constructionSite.structureType === STRUCTURE_ROAD
					|| loc.structure && loc.structure.structureType === STRUCTURE_ROAD) {
						costMatrix.set(loc.x, loc.y, 1);
					}

				if (!costMatrix.get(loc.x, loc.y)) {
					if (loc.terrain === 'swamp') {
						costMatrix.set(loc.x, loc.y, 10);
					} else if (loc.terrain === 'plain') {
						costMatrix.set(loc.x, loc.y, 5);
					}
				}
			}));

			return costMatrix;
		},
	} as PathFinderOpts);

	let room = Game.rooms[start.roomName];
	path.path.forEach((pos: RoomPosition) => {
		if (!room.lookForAt<StructureRoad>(STRUCTURE_ROAD, pos).length) {
			room.createConstructionSite(pos, STRUCTURE_ROAD);
		}
	});

	return;
};

export { constructRoadBetweenPoints };
