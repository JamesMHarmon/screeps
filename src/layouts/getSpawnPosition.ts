import { Layout } from './layout';

export function getSpawnPosition(layout: Layout) {
	let spawnIndexY = layout.findIndex(lr => _.any(lr, t => t === 'spawn'));
	let spawnIndexX = layout[spawnIndexY].findIndex(t => t === 'spawn');

	return {
		x: spawnIndexX,
		y: spawnIndexY,
	};
};
