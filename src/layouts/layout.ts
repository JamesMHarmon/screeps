import { LayoutRow } from './layoutRow';

export type Layout = LayoutRow[];
