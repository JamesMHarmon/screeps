import { Layout } from './layout';
import { getSpawnPosition } from './getSpawnPosition';
import { log } from '../components/support/log';

export function findLayoutPlacement(room: Room, layout: Layout): RoomPosition | undefined {
	let spawn: Spawn = room.find<Spawn>(FIND_MY_SPAWNS)[0];

	if (spawn) {
		let spawnPosition = getSpawnPosition(layout);

		let spawnPosX = spawn.pos.x;
		let spawnPosY = spawn.pos.y;

		if (_.all(layout, (lr, y) => {
			return _.all(lr, (t, x) => {
				let canPlace = t === null || Game.map.getTerrainAt(
					x + spawnPosX - spawnPosition.x,
					y + spawnPosY - spawnPosition.y, room.name
				) !== 'wall';

				if (!canPlace) {
					log.info('Could not place layout: ', t, x + spawnPosX - spawnPosition.x, y + spawnPosY - spawnPosition.y);
				}

				return canPlace;
			});
		})) {
			return new RoomPosition(spawnPosX - spawnPosition.x, spawnPosY - spawnPosition.y, room.name);
		}

	}

	return undefined;
};
