import { Layout } from './layout';

let asteroidsLayout: Layout = [
	[	null,			null,			'extension',	'road',			'extension',	null,			null],
	[	null,			null,			'extension',	'road',			'extension',	'extension',	null],
	[	'extension',	'extension',	'road',			'tower',		'road',			'extension',	'extension'],
	[	'extension',	'road',			'spawn',		'road',			'link',			'road',			'extension'],
	[	'extension',	'extension',	'road',			'storage',		'road',			'extension',	'extension'],
	[	null,			'extension',	'road',			'road',			'road',			'extension',	null],
	[	null,			'extension',	'road',			'container',	'road',			'extension',	null],
];

export { asteroidsLayout };
