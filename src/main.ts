import * as Config from './config/config';
import { getAllMySpawns } from './room/mySpawns';
import { Operation } from './operation/operation';
import { ExtractEnergyOperation } from './operation/extractEnergyOperation';
import { BuildAndRepairStructuresOperation } from './operation/buildAndRepairStructuresOperation';
import { UpgradeControllerOperation } from './operation/upgradeControllerOperation';
import { InfrastructureZoningOperation } from './operation/infrastructureZoningOperation';
import { TowerDefenseOperation } from './operation/towerDefenseOperation';
import { PowerStructureOperation } from './operation/powerStructure.operation';
import { ReinforceWallsOperation } from './operation/reinforceWallsOperation';
import { asteroidsLayout } from './layouts/asteroids.layout';
import { BasicCreepFactory } from './creepFactory/basicCreepFactory';
import { BasicCreepPool } from './creepFactory/basicCreepPool';
import { CreepPool } from './creepFactory/creepPool';
import { log } from './components/support/log';

// Any code written outside the `loop()` method is executed only when the
// Screeps system reloads your script.
// Use this bootstrap wisely. You can cache some of your stuff to save CPU.
// You should extend prototypes before the game loop executes here.

// This is an example for using a config variable from `config.ts`.
if (Config.USE_PATHFINDER) {
	PathFinder.use(true);
}

/**
 * Screeps system expects this 'loop' method in main.js to run the
 * application. If we have this line, we can be sure that the globals are
 * bootstrapped properly and the game loop is executed.
 * http://support.screeps.com/hc/en-us/articles/204825672-New-main-loop-architecture
 *
 * @export
 */

export function loop() {
	if (Memory.disableLoop) {
		return;
	}

	let creepFactory = Memory.serializedState
		&& Memory.serializedState.creepFactory
		&& BasicCreepFactory.deserialize(Memory.serializedState.creepFactory);

	let room = _.find(Game.rooms);

	if (!creepFactory) {
		creepFactory = getAllMySpawns().map(s => new BasicCreepFactory(s))[0];
	}

	let operations: Operation[];
	let basicCreepFactory = creepFactory as BasicCreepFactory;
	let creepPool = new BasicCreepPool(_.values<Creep>(Game.creeps), basicCreepFactory);

	try {
		operations = deserializeOperations(creepPool);
	} catch (error) {
		log.error('Failed to deserialize operations');
		operations = createOperations(creepPool, room);
	}

	try {
		operations.forEach(o => o.run());
		creepFactory.run();
	} catch (error) {
		log.error('Failed to run task', error);
	}

	try {
		Memory.serializedState = {
			creepFactory: creepFactory.serialize(),
			operations: operations.map(o => o.serialize()),
		};
	} catch (error) {
		log.error('Failed to serialize operation', error);
	}
}

function deserializeOperations(creepPool: CreepPool): Operation[] {
	let serializedOperations = Memory.serializedState && Memory.serializedState.operations;

	if (!serializedOperations) {
		throw 'Serialized operations not found';
	}

	return serializedOperations.map((o: any) => {
		if (o.type === 'BuildStructures') {
			return BuildAndRepairStructuresOperation.deserialize(o, creepPool);
		} else if (o.type === 'ExtractEnergy') {
			return ExtractEnergyOperation.deserialize(o, creepPool);
		} else if (o.type === 'InfrastructureZoning') {
			return InfrastructureZoningOperation.deserialize(o);
		} else if (o.type === 'PowerStructure') {
			return PowerStructureOperation.deserialize(o, creepPool);
		} else if (o.type === 'UpgradeController') {
			return UpgradeControllerOperation.deserialize(o, creepPool);
		} else if (o.type === 'TowerDefense') {
			return TowerDefenseOperation.deserialize(o);
		} else if (o.type === 'ReinforceWalls') {
			return ReinforceWallsOperation.deserialize(o, creepPool);
		} else {
			return;
		}
	});
};

function createOperations(creepPool: CreepPool, room: Room): Operation[] {
	let operations: Operation[] = [];
	let spawn = room.find<Spawn>(FIND_MY_SPAWNS)[0];
	let container = spawn.pos.findInRange<Container>(FIND_STRUCTURES, 4, {
		filter: (s: Structure) => s.structureType === STRUCTURE_CONTAINER,
	})[0];
	let storage = spawn.pos.findInRange<Storage>(FIND_STRUCTURES, 4, {
		filter: (s: Structure) => s.structureType === STRUCTURE_STORAGE,
	})[0];
	let energySource = storage || container || spawn;

	room.find<Source>(FIND_SOURCES).forEach((source, i) => {
		operations.push(new ExtractEnergyOperation(room, source, energySource, creepPool));

		if (i === 0) {
			operations.push(new PowerStructureOperation(room, energySource, creepPool));
		}
	});

	operations.push(new BuildAndRepairStructuresOperation(room, energySource, creepPool));

	operations.push(new InfrastructureZoningOperation(room, asteroidsLayout));

	operations.push(new UpgradeControllerOperation(room, energySource, creepPool));

	operations.push(new TowerDefenseOperation(room));

	operations.push(new ReinforceWallsOperation(room, energySource, creepPool));

	return operations;
}

export function reset() {
	delete Memory.serializedState;
	let creepNames: string[] = _.keys(Memory.creeps);
	creepNames.forEach(creepName => {
		if (Game.creeps[creepName]) {
			Memory.creeps[creepName].unclaimed = true;
		} else {
			delete Memory.creeps[creepName];
		}
	});

	return 'Reset Game State';
}

export function resetConstructionSites() {
	_.values<Room>(Game.rooms).forEach(r => {
		r.find<ConstructionSite>(FIND_CONSTRUCTION_SITES).forEach(cs => cs.remove());
	});
}
