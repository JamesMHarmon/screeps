interface Memory {
	uuid: number;
	log: any;
	serializedState: any;
	disableLoop: boolean;
}

interface Global {
	log?: any;
}

type TransferableEnitity =
	Creep
	| StructureLab
	| StructureStorage
	| StructureTerminal
	| StructureContainer
	| StructureSpawn;

type Predicate<T> = (arg1: T) => boolean;
type Action<T> = (arg1: T) => void;

declare var global: Global;

declare var require: any;
