let currierCreepSmallBlueprint = {
	bodyParts: [
		{
			max: 2,
			min: 1,
			part: MOVE,
			weight: 10,
		},
		{
			max: 4,
			min: 1,
			part: CARRY,
			weight: 19,
		},
	],
};

export { currierCreepSmallBlueprint };
