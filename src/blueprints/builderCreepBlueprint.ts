let builderCreepBlueprint = {
	bodyParts: [
		{
			max: Infinity,
			min: 1,
			part: MOVE,
			weight: 98,
		},
		{
			max: Infinity,
			min: 1,
			part: WORK,
			weight: 99,
		},
		{
			max: Infinity,
			min: 1,
			part: CARRY,
			weight: 100,
		},
	],
};

export { builderCreepBlueprint };
