let currierCreepBlueprint = {
	bodyParts: [
		{
			max: Infinity,
			min: 1,
			part: MOVE,
			weight: 10,
		},
		{
			max: Infinity,
			min: 1,
			part: CARRY,
			weight: 19,
		},
	],
};

export { currierCreepBlueprint };
