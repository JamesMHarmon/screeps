let upgraderCreepBlueprint = {
	bodyParts: [
		{
			max: Infinity,
			min: 1,
			part: MOVE,
			weight: 1,
		},
		{
			max: Infinity,
			min: 1,
			part: WORK,
			weight: 3,
		},
		{
			max: Infinity,
			min: 1,
			part: CARRY,
			weight: 2,
		},
	],
};

export { upgraderCreepBlueprint };
