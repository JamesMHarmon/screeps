let extractorCreepBlueprint = {
	bodyParts: [
		{
			max: 3,
			min: 1,
			part: MOVE,
			weight: 1,
		},
		{
			max: 5,
			min: 2,
			part: WORK,
			weight: 5,
		},
		{
			max: 1,
			min: 1,
			part: CARRY,
			weight: 1,
		},
	],
};

export { extractorCreepBlueprint };
