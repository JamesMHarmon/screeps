let offroadBuilderCreepBlueprint = {
	bodyParts: [
		{
			max: Infinity,
			min: 1,
			part: MOVE,
			weight: 4,
		},
		{
			max: Infinity,
			min: 1,
			part: WORK,
			weight: 1,
		},
		{
			max: Infinity,
			min: 1,
			part: CARRY,
			weight: 3,
		},
	],
};

export { offroadBuilderCreepBlueprint };
