import { BodyPartBlueprint } from './bodyPartBlueprint';

export type CreepBlueprint = {
	bodyParts: BodyPartBlueprint[]
};
