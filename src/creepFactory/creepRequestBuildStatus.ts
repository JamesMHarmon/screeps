export type CreepRequestBuildStatus = 'Queued' | 'Fabricating' | 'Complete';
