import { CreepRequest } from './creepRequest';

export interface CreepPool {
	claimCreep(name: string): Creep | undefined;

	getCreep(request: CreepRequest): Creep | undefined;
}
