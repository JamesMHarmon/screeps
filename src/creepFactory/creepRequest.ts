import { CreepBlueprint } from './creepBlueprint';
import { CreepRequestPriority } from './creepRequestPriority';

export type CreepRequest = {
	blueprint: CreepBlueprint,
	priority: CreepRequestPriority
};
