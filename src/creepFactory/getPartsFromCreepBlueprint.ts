import { CreepBlueprint } from './creepBlueprint';

export function getPartsFromCreepBlueprint(blueprint: CreepBlueprint, energy: number): string[] {
	let partRequests = mapBlueprintToPartRequests(blueprint);

	let remainingEnergy = energy - _.sum(partRequests, pr => {
		return pr.numParts * pr.energyCost;
	});

	let bestMatchedPart: BodyPartRequest | undefined;
	while (bestMatchedPart = determineNextPart(partRequests, remainingEnergy)) {
		bestMatchedPart.numParts++;
		remainingEnergy -= bestMatchedPart.energyCost;
	}

	let partResult = _.flatten(partRequests.map(pr => _.times(pr.numParts, () => pr.bodyPart)));

	return partResult;
};

type BodyPartRequest = {
	bodyPart: string,
	energyCost: number,
	maxParts: number,
	numParts: number,
	percent: number,
};

function mapBlueprintToPartRequests(blueprint: CreepBlueprint): BodyPartRequest[] {
	let bodyParts = blueprint.bodyParts;
	let totalWeight = _.sum(bodyParts, d => d.weight);

	return bodyParts.map(bp => {
		return {
			bodyPart: bp.part,
			energyCost: BODYPART_COST[bp.part],
			maxParts: bp.max,
			numParts: bp.min,
			percent: bp.weight / totalWeight,
		};
	});
}

function determineNextPart(parts: BodyPartRequest[], energy: number): BodyPartRequest | undefined {
	let possibleParts = parts.filter(pr => pr.energyCost <= energy && pr.numParts < pr.maxParts);

	let partsWithScores = possibleParts.map(pr => {
		let indexOfPart = _.indexOf(parts, pr);
		let partClone = _.cloneDeep(parts);
		partClone[indexOfPart].numParts++;

		return {
			partRequest: pr,
			score: calculatePartScore(partClone),
		};
	});

	let bestScore = _.sortBy(partsWithScores, pws => pws.score)[0];

	return bestScore && bestScore.partRequest;
}

function calculatePartScore(parts: BodyPartRequest[]): number {
	let totalParts = _.sum(parts, pr => pr.numParts);

	return _.sum(parts, pr => {
		return Math.abs(pr.percent - (pr.numParts / totalParts)) * pr.percent;
	});
}
