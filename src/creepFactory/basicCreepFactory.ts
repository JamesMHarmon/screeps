import { log } from '../components/support/log';
import { CreepRequest } from './creepRequest';
import { CreepFactory } from './creepFactory';
import { getPartsFromCreepBlueprint } from './getPartsFromCreepBlueprint';

export class BasicCreepFactory implements CreepFactory {
	public static deserialize(serializedFactory: any): BasicCreepFactory | undefined {
		let spawn = Game.getObjectById<Spawn>(serializedFactory.spawn);

		if (!spawn) {
			log.error('Could not find spawn with id: ' + serializedFactory.spawn);
			return undefined;
		}

		return new BasicCreepFactory(spawn);
	}

	private spawn: Spawn;

	private requestPool: CreepRequest[] = [];

	constructor(spawn: Spawn) {
		this.spawn = spawn;
	}

	public requestCreep(request: CreepRequest): undefined {
		this.requestPool.push(request);
		return;
	}

	public run(): undefined {
		let nextCreepInQueue = this.getNextCreepInQueue();

		if (!nextCreepInQueue) {
			log.info('No creeps in queue');
			return;
		}

		let spawn = this.spawn;
		let extensions = spawn.room.find<StructureExtension>(FIND_MY_STRUCTURES, {
			filter: (s: Structure) => s.structureType === STRUCTURE_EXTENSION,
		});
		let availableEnergy = spawn.energy + _.sum(extensions, e => e.energy);

		if (availableEnergy < 300) {
			log.info('Not enough energy to spawn: ' + availableEnergy);
			return;
		}

		let creepParts = getPartsFromCreepBlueprint(nextCreepInQueue.blueprint, availableEnergy);
		let creep = this.spawn.createCreep(creepParts);

		if (typeof creep === 'number') {
			log.info('Unable to spawn creep: ' + creep, JSON.stringify(creepParts));
		}

		return;
	}

	public serialize() {
		return {
			spawn: this.spawn.id,
		};
	}

	private getNextCreepInQueue(): CreepRequest | undefined {
		let requestsGroupedByPriority = _.groupBy(this.requestPool, r => r.priority);

		/* tslint:disable */
		return (requestsGroupedByPriority['High']
			|| requestsGroupedByPriority['Med']
			|| requestsGroupedByPriority['Low']
			|| []
		)[0];
		/* tslint:enable */
	}
};
