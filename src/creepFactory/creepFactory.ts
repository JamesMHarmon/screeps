import { CreepRequest } from './creepRequest';

export interface CreepFactory {

	requestCreep(request: CreepRequest): undefined;

	run(): undefined;
};
