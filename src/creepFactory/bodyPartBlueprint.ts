export type BodyPartBlueprint = {
	part: string,
	weight: number,
	min: number,
	max: number,
};
