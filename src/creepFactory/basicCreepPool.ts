import { CreepFactory } from './CreepFactory';
import { CreepRequest } from './creepRequest';
import { CreepPool } from './creepPool';

export class BasicCreepPool implements CreepPool {
	public static creepMatchesRequest(request: CreepRequest, creep: Creep): boolean {
		let requestedPartNames = _.map(request.blueprint.bodyParts, bp => bp.part);
		let numberOfPartsRequestedByName = _.reduce(request.blueprint.bodyParts, (acc: any, bp) => {
			acc[bp.part] = bp.min;
			return acc;
		}, {});
		let creepPartsGrouped = _.groupBy(creep.body.map(bp => bp.type));

		return _.every(requestedPartNames, partName => {
			let numberOfPartsOnCreep = creepPartsGrouped[partName] && creepPartsGrouped[partName].length;
			let numberOfPartsRequested = numberOfPartsRequestedByName[partName];
			return numberOfPartsOnCreep >= numberOfPartsRequested;
		});
	}

	private creepFactory: CreepFactory;

	private creeps: Creep[];

	constructor(creeps: Creep[], creepFactory: CreepFactory) {
		this.creepFactory = creepFactory;
		this.creeps = creeps;
	}

	public claimCreep(name: string): Creep | undefined {
		return _.remove(this.creeps, c => c.name === name)[0];
	}

	public getCreep(request: CreepRequest): Creep | undefined {
		let creep: Creep | undefined = _.find(this.creeps, c => BasicCreepPool.creepMatchesRequest(request, c));

		if (!creep) {
			this.creepFactory.requestCreep(request);
		}

		return creep;
	}
};
