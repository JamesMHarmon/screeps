export function forMySpawnsInRoom(room: Room, action: Action<Spawn>): void {
	getMySpawnsInRoom(room).forEach(action);
};

export function getMySpawnsInRoom(room: Room): Spawn[] {
	return room.find<Spawn>(FIND_MY_SPAWNS);
};

export function getAllMySpawns(): Spawn[] {
	return _.values<Spawn>(Game.spawns);
};

export function forAllMySpawns(each: Action<Spawn>): void {
	getAllMySpawns().forEach(each);
};
