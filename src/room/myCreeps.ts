export function forMyCreepsInRoom(room: Room, action: Action<Creep>): void {
	getMyCreepsInRoom(room).forEach(action);
};

export function getMyCreepsInRoom(room: Room): Creep[] {
	return room.find<Creep>(FIND_MY_CREEPS);
};

export function getAllMyCreeps(): Creep[] {
	return _.values<Creep>(Game.creeps);
};

export function forAllMyCreeps(each: Action<Creep>): void {
	getAllMyCreeps().forEach(each);
};
