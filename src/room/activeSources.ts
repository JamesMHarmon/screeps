export function forActiveSourcesInRoom(room: Room, action: Action<Source>): void {
	getActiveSourcesInRoom(room).forEach(action);
};

export function getActiveSourcesInRoom(room: Room): Source[] {
	return room.find<Source>(FIND_SOURCES_ACTIVE);
};
