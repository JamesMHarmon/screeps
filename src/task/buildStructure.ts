import { buildOrMoveTo } from '../creep/buildOrMoveTo';
import { WithdrawTransferHarvestOrMoveTo } from './withdrawTransferHarvestOrMoveTo';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';

export class BuildStructure {
	public static execute(creep: Creep, source: TransferableEnitity): TaskResult {
		if (creep.carry.energy === creep.carryCapacity) {
			creep.memory.harvesting = false;
		} else if (creep.carry.energy === 0) {
			creep.memory.harvesting = true;
		}

		if (creep.memory.harvesting) {
			WithdrawTransferHarvestOrMoveTo.execute(creep, source, RESOURCE_ENERGY);
		} else {
			let constructionSite = creep.room.find<ConstructionSite>(FIND_MY_CONSTRUCTION_SITES)[0];

			if (constructionSite) {
				buildOrMoveTo(creep, constructionSite);
			} else {
				return TaskResultFactory.noWork();
			}
		}

		return TaskResultFactory.success();
	}
};
