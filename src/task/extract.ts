import { harvestOrMoveTo } from '../creep/harvestOrMoveTo';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';

export class Extract {
	public static execute(creep: Creep, source: Source): TaskResult {
		harvestOrMoveTo(creep, source);

		return TaskResultFactory.success();
	}
};
