export type CurrierRequest = {
	creep: Creep,
	source: TransferableEnitity,
	target: Structure | Creep,
	resourceType: string
}
