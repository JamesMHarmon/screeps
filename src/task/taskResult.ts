import { TaskError } from './taskError';

export type TaskResult = {
	error: TaskError,
	success: boolean,
};
