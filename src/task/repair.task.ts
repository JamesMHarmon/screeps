import { repairOrMoveTo } from '../creep/repairOrMoveTo';
import { WithdrawTransferHarvestOrMoveTo } from './withdrawTransferHarvestOrMoveTo';
import { Task } from './task';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';
export class RepairTask implements Task {

	public static deserialize(serializedTask: any): RepairTask {
		let creep: Creep = Game.creeps[serializedTask.creep];
		let isComplete: boolean = serializedTask.isComplete;
		let isEnergizing: boolean = serializedTask.isEnergizing;
		let targetHitpoints: number = serializedTask.targetHitpoints;
		let source: TransferableEnitity = Game.getObjectById(serializedTask.source) as TransferableEnitity;
		let target: Structure = Game.getObjectById(serializedTask.target) as Structure;
		return new RepairTask(creep, target, source, targetHitpoints, isEnergizing, isComplete);
	}

	public isComplete: boolean;

	public creep: Creep;

	public target: Structure;

	private source: TransferableEnitity;

	private isEnergizing: boolean;

	private targetHitpoints: number;

	constructor(
		creep: Creep,
		target: Structure,
		source: TransferableEnitity,
		targetHitpoints?: number,
		isEnergizing?: boolean,
		isComplete?: boolean,
	) {
		this.creep = creep;
		this.target = target;
		this.source = source;

		this.targetHitpoints = targetHitpoints || target.hitsMax;
		this.isEnergizing = isEnergizing !== undefined ? isEnergizing : false;
		this.isComplete = isComplete !== undefined ? isComplete : false;
	}

	public run(): TaskResult {
		let creep = this.creep;
		let target = this.target;

		if (!creep || !target) {
			this.isComplete = true;
			return TaskResultFactory.noWork();
		}

		if (this.isComplete || target.hits >= this.targetHitpoints) {
			this.isComplete = true;
			return TaskResultFactory.noWork();
		}

		if (creep.carry.energy === creep.carryCapacity) {
			this.isEnergizing = false;
		} else if (creep.carry.energy === 0) {
			this.isEnergizing = true;
		}

		if (this.isEnergizing) {
			WithdrawTransferHarvestOrMoveTo.execute(creep, this.source, RESOURCE_ENERGY);
		} else {
			repairOrMoveTo(creep, target);
		}

		return TaskResultFactory.success();
	}

	public serialize(): Object {
		return {
			creep: this.creep.name,
			isComplete: this.isComplete,
			isEnergizing: this.isEnergizing,
			source: this.source.id,
			target: this.target.id,
			targetHitpoints: this.targetHitpoints,
		};
	}
};
