import { TaskResult } from './taskResult';

export interface Task {
	isComplete: boolean;

	run: (creep: Creep) => TaskResult;

	serialize: () => any;
};
