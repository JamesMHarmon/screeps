import { repairOrMoveTo } from '../creep/repairOrMoveTo';
import { WithdrawTransferHarvestOrMoveTo } from './withdrawTransferHarvestOrMoveTo';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';

export class Repair {
	public static execute(creep: Creep, source: TransferableEnitity): TaskResult {

		if (creep.carry.energy === creep.carryCapacity) {
			creep.memory.harvesting = false;
		} else if (creep.carry.energy === 0) {
			creep.memory.harvesting = true;
		}

		if (creep.memory.harvesting) {
			WithdrawTransferHarvestOrMoveTo.execute(creep, source, RESOURCE_ENERGY);
		} else {
			let structure = creep.room.find<Structure>(FIND_STRUCTURES, {
				filter: Repair.isRepairableNonWallStructure,
			})[0];

			if (structure) {
				repairOrMoveTo(creep, structure);
			} else {
				return TaskResultFactory.noWork();
			}
		}

		return TaskResultFactory.success();
	}

	private static isRepairableNonWallStructure(structure: Structure) {
		return structure.structureType !== STRUCTURE_WALL
			&& structure.structureType !== STRUCTURE_RAMPART
			&& structure.hits < structure.hitsMax;
	}
};
