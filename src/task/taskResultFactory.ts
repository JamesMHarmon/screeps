import { TaskResult } from './taskResult';

export class TaskResultFactory {
	public static success(): TaskResult {
		return {
			error: 'NO_ERROR',
			success: true,
		};
	}

	public static noWork(): TaskResult {
		return {
			error: 'NO_WORK',
			success: true,
		};
	}
};
