import { withdrawOrMoveTo } from '../creep/withdrawOrMoveTo';
import { harvestOrMoveTo } from '../creep/harvestOrMoveTo';
import { transferFromOrMoveTo } from '../creep/transferFromOrMoveTo';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';

export class WithdrawTransferHarvestOrMoveTo {
	public static execute(creep: Creep, source: Structure, resourceType: string): TaskResult;
	public static execute(creep: Creep, source: TransferableEnitity, resourceType: string): TaskResult;
	public static execute(creep: Creep, source: Source | Mineral, resourceType: string): TaskResult;
	public static execute(creep: Creep, source: any, resourceType: string): TaskResult {
			let droppedResource = creep.pos.findInRange<Resource>(FIND_DROPPED_RESOURCES, 1, {
				filter: (r: Resource) => r.resourceType === resourceType,
			});

			if (droppedResource.length) {
				creep.pickup(droppedResource[0]);
			}

			if (typeof source.transfer === 'function') {
				transferFromOrMoveTo(creep, source as TransferableEnitity, resourceType);
			} else if (typeof source.ticksToRegenerate === 'number') {
				harvestOrMoveTo(creep, source as Source);
			} else {
				withdrawOrMoveTo(creep, source, resourceType);
			}

			return TaskResultFactory.success();
	}
};
