import { transferOrMoveTo } from '../creep/transferOrMoveTo';
import { WithdrawTransferHarvestOrMoveTo } from '../task/withdrawTransferHarvestOrMoveTo';
import { CurrierRequest } from './currierRequest';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';

export class Currier {
	public static execute(request: CurrierRequest): TaskResult {
		let creep = request.creep;

		if (creep.carry.energy === creep.carryCapacity) {
			creep.memory.harvesting = false;
		} else if (creep.carry.energy === 0) {
			creep.memory.harvesting = true;
		}

		if (creep.memory.harvesting) {
			WithdrawTransferHarvestOrMoveTo.execute(creep, request.source, request.resourceType);
		} else {
			transferOrMoveTo(creep, request.target, request.resourceType);
		}

		return TaskResultFactory.success();
	}
};
