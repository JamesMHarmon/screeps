import { transferOrMoveTo } from '../creep/transferOrMoveTo';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';

export class UpgradeControllerTask {
	public static execute(creep: Creep): TaskResult {
		let controller = creep.room.controller;

		if (!controller) {
			return TaskResultFactory.noWork();
		}

		transferOrMoveTo(creep, controller, RESOURCE_ENERGY);

		return TaskResultFactory.success();
	}
};
