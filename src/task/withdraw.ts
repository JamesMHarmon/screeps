import { withdrawOrMoveTo } from '../creep/withdrawOrMoveTo';
import { TaskResult } from './taskResult';
import { TaskResultFactory } from './taskResultFactory';

export class Extract {
	public static execute(creep: Creep, source: Structure, resourceType: string): TaskResult {
		withdrawOrMoveTo(creep, source, resourceType);

		return TaskResultFactory.success();
	}
};
