import { Currier } from '../task/currier';
import { UpgradeControllerTask } from '../task/upgradeControllerTask';
import { CreepPool } from '../creepFactory/creepPool';
import { Operation } from '../operation/operation';
import { currierCreepBlueprint } from '../blueprints/currierCreepBlueprint';
import { upgraderCreepBlueprint } from '../blueprints/upgraderCreepBlueprint';
import { constructRoadBetweenPoints } from '../structures/constructRoadBetweenPoints';
import { everyXTicks } from '../ticks/everyXTicks';
import { log } from '../components/support/log';

export class UpgradeControllerOperation implements Operation {

	public static deserialize(serializedOperation: any, creepPool: CreepPool) {
		let room = Game.rooms[serializedOperation.room];
		let energySource = Game.getObjectById<TransferableEnitity>(serializedOperation.energySource);
		let upgraderCreep = creepPool.claimCreep(serializedOperation.upgraderCreep);
		let currierCreeps = (serializedOperation.currierCreeps || [])
			.map((name: string) => creepPool.claimCreep(name))
			.filter((c: Creep) => c);

		return new UpgradeControllerOperation(
			room,
			energySource as TransferableEnitity,
			creepPool,
			upgraderCreep,
			currierCreeps);
	}

	private creepPool: CreepPool;

	private room: Room;

	private energySource: TransferableEnitity;

	private upgraderCreep: Creep;

	private currierCreeps: Creep[];

	constructor(
		room: Room,
		energySource: TransferableEnitity,
		creepPool: CreepPool,
		upgraderCreep?: Creep,
		currierCreeps?: Creep[]) {
			if (!room || !creepPool || !energySource) {
				log.error('UpgradeControllerOperation created without room or creepPool', room, creepPool, energySource);
			}

			this.creepPool = creepPool;
			this.room = room;
			this.energySource = energySource;
			this.currierCreeps = currierCreeps || [];

			if (upgraderCreep) {
				this.upgraderCreep = upgraderCreep;
			}
	}

	public run() {
		let upgraderCreep = this.getOrRequestUpgraderCreep();
		let currierCreeps = this.getOrRequestCurrierCreeps();
		let controller = this.room.controller;

		everyXTicks(() => this.constructRoad(), 100);

		if (upgraderCreep && controller) {
			UpgradeControllerTask.execute(upgraderCreep);
		}

		currierCreeps.forEach(currierCreep => {
			if (!upgraderCreep) {
				return;
			}

			Currier.execute({
				creep: currierCreep,
				resourceType: RESOURCE_ENERGY,
				source: this.energySource,
				target: upgraderCreep,
			});
		});
	}

	public serialize() {
		let currierCreeps = this.currierCreeps || [];
		let upgraderCreep = this.upgraderCreep || {};

		return {
			currierCreeps: currierCreeps.map(c => c.name),
			energySource: this.energySource.id,
			room: this.room.name,
			type: 'UpgradeController',
			upgraderCreep: upgraderCreep.name,
		};
	}

	private getOrRequestUpgraderCreep(): Creep | undefined {
		if (!this.upgraderCreep) {
			let creep =  this.requestUpgraderCreep();
			if (creep) {
				this.upgraderCreep = creep;
			}
		}

		return this.upgraderCreep;
	}

	private requestUpgraderCreep(): Creep | undefined {
		return this.creepPool.getCreep({
			blueprint: upgraderCreepBlueprint,
			priority: 'Low',
		});
	}

	private getOrRequestCurrierCreeps(): Creep[] {
		if (this.currierCreeps.length < 1) {
			let creep =  this.requestCurrierCreep();
			if (creep) {
				this.currierCreeps.push(creep);
			}
		}

		return this.currierCreeps;
	}

	private requestCurrierCreep(): Creep | undefined {
		return this.creepPool.getCreep({
			blueprint: currierCreepBlueprint,
			priority: 'Low',
		});
	}

	private constructRoad(): undefined {
		let controller = this.room.controller;

		if (!this.energySource || !controller) {
			return;
		}

		constructRoadBetweenPoints(this.energySource.pos, controller.pos);

		return;
	}
};
