import { everyXTicks } from '../ticks/everyXTicks';
import { Operation } from '../operation/operation';
import { log } from '../components/support/log';
import { Layout } from '../layouts/layout';
import { findLayoutPlacement } from '../layouts/findLayoutPlacement';

export class InfrastructureZoningOperation implements Operation {

	public static deserialize(serializedOperation: any) {
		let room = Game.rooms[serializedOperation.room];
		let layout = serializedOperation.layout;

		return new InfrastructureZoningOperation(room, layout);
	}

	private room: Room;

	private layout: Layout;

	constructor(room: Room, layout: Layout) {
		if (!room) {
			log.error('InfrastructureZoningOperation created without room');
		}

		this.room = room;
		this.layout = layout;
	}

	public run() {
		everyXTicks(() => {
			let layoutPos = findLayoutPlacement(this.room, this.layout);

			if (!layoutPos) {
				return;
			}

			this.layout.forEach((lr, y) => {
				lr.forEach((t, x) => {
					if (t && layoutPos) {
						this.room.createConstructionSite(layoutPos.x + x, layoutPos.y + y, t);
					}
				});
			});
		}, 307);
	}

	public serialize() {
		return {
			layout: this.layout,
			room: this.room.name,
			type: 'InfrastructureZoning',
		};
	}
}
