export interface Operation {
	run: () => void;

	serialize: () => Object;
};
