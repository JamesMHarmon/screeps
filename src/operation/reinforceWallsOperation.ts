import { RepairTask } from '../task/repair.task';
import { Task } from '../task/task';
import { CreepPool } from '../creepFactory/creepPool';
import { Operation } from '../operation/operation';
import { offroadBuilderCreepBlueprint } from '../blueprints/offroadBuilderCreepBlueprint';
import { log } from '../components/support/log';

type StructureWallOrRampart = StructureWall | StructureRampart;

export class ReinforceWallsOperation implements Operation {

	public static deserialize(serializedOperation: any, creepPool: CreepPool) {
		let room = Game.rooms[serializedOperation.room];
		let energySource = Game.getObjectById<TransferableEnitity>(serializedOperation.energySource);
		let builderCreep = creepPool.claimCreep(serializedOperation.builderCreep);
		let tasks = (serializedOperation.tasks || [])
			.filter((t: any) => serializedOperation.builderCreep === t.creep)
			.map((st: any) => RepairTask.deserialize(st));

		return new ReinforceWallsOperation(
			room,
			energySource as TransferableEnitity,
			creepPool,
			builderCreep,
			tasks);
	}

	private creepPool: CreepPool;

	private room: Room;

	private energySource: TransferableEnitity;

	private builderCreep: Creep;

	private tasks: Task[];

	constructor(
		room: Room,
		energySource: TransferableEnitity,
		creepPool: CreepPool,
		builderCreep?: Creep,
		tasks?: RepairTask[],
	) {
			if (!room || !creepPool || !energySource) {
				log.error('ReinforceWallsOperation created without room or creepPool', room, creepPool, energySource);
			}

			this.creepPool = creepPool;
			this.room = room;
			this.energySource = energySource;
			this.tasks = tasks || [];

			if (builderCreep) {
				this.builderCreep = builderCreep;
			}
	}

	public run() {
		let builderCreep = this.getOrRequestbuilderCreep();

		if (!builderCreep) {
			return;
		}

		let hasTask = _.any<RepairTask>(this.tasks, (task: RepairTask) => {
			return task.creep === builderCreep;
		});

		if (!hasTask) {
			let wallToRepair = this.getNearestWallInDisrepair(builderCreep.pos);

			if (wallToRepair) {
				let targetHitpoints = wallToRepair.hits + 40000;
				let repairTask = new RepairTask(
					builderCreep,
					wallToRepair,
					this.energySource,
					targetHitpoints);
				this.tasks.push(repairTask);
			}
		}

		this.tasks.forEach((t: RepairTask) => t.run());
	}

	public serialize() {
		return {
			builderCreep: (this.builderCreep || {}).name,
			energySource: this.energySource.id,
			room: this.room.name,
			tasks: this.tasks
				.filter(t => !t.isComplete)
				.map(t => t.serialize()),
			type: 'ReinforceWalls',
		};
	}

	private getOrRequestbuilderCreep(): Creep | undefined {
		if (!this.builderCreep) {
			let creep =  this.requestbuilderCreep();
			if (creep) {
				this.builderCreep = creep;
			}
		}

		return this.builderCreep;
	}

	private requestbuilderCreep(): Creep | undefined {
		return this.creepPool.getCreep({
			blueprint: offroadBuilderCreepBlueprint,
			priority: 'Low',
		});
	}

	private getNearestWallInDisrepair(pos: RoomPosition): StructureWallOrRampart | undefined {
		let room = Game.rooms[pos.roomName];
		let wallsAndRamparts: StructureWallOrRampart[] = room.find<StructureWallOrRampart>(FIND_STRUCTURES, {
			filter: (s: Structure) => {
				return s.structureType === STRUCTURE_WALL || s.structureType === STRUCTURE_RAMPART;
			},
		});

		if (!wallsAndRamparts.length) {
			return;
		}

		let wallToRepair = _.sortBy(wallsAndRamparts, (s: StructureWallOrRampart) => s.hits)[0];

		return wallToRepair;
	}
};
