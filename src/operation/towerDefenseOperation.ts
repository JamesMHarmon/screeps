import { Operation } from '../operation/operation';
import { log } from '../components/support/log';

export class TowerDefenseOperation implements Operation {

	public static deserialize(serializedOperation: any) {
		let room = Game.rooms[serializedOperation.room];

		return new TowerDefenseOperation(room);
	}

	private room: Room;

	constructor(room: Room) {
		if (!room) {
			log.error('TowerDefenseOperation created without room or creepPool', room);
		}

		this.room = room;
	}

	public run() {
		this.room.find<Tower>(FIND_MY_STRUCTURES, {
			filter: (s: Structure) => s.structureType === STRUCTURE_TOWER,
		}).forEach(tower => {
			let nearestHostile = tower.pos.findClosestByRange<Creep>(FIND_HOSTILE_CREEPS);

			if (nearestHostile) {
				tower.attack(nearestHostile);
			}
		});
	}

	public serialize() {
		return {
			room: this.room.name,
			type: 'TowerDefense',
		};
	}
};
