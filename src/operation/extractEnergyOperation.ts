import { Extract } from '../task/extract';
import { Currier } from '../task/currier';
import { CreepPool } from '../creepFactory/creepPool';
import { Operation } from '../operation/operation';
import { currierCreepBlueprint } from '../blueprints/currierCreepBlueprint';
import { extractorCreepBlueprint } from '../blueprints/extractorCreepBlueprint';
import { constructRoadBetweenPoints } from '../structures/constructRoadBetweenPoints';
import { everyXTicks } from '../ticks/everyXTicks';
import { log } from '../components/support/log';

export class ExtractEnergyOperation implements Operation {

	public static deserialize(serializedOperation: any, creepPool: CreepPool) {
		let room = Game.rooms[serializedOperation.room];
		let source = Game.getObjectById<Source>(serializedOperation.source);
		let target = Game.getObjectById<Creep | Structure>(serializedOperation.target);
		let extractorCreep = creepPool.claimCreep(serializedOperation.extractorCreep);
		let currierCreeps = (serializedOperation.currierCreeps || [])
			.map((name: string) => creepPool.claimCreep(name))
			.filter((c: Creep) => c);

		return new ExtractEnergyOperation(
			room,
			source as Source,
			target as Creep | Structure,
			creepPool,
			extractorCreep,
			currierCreeps);
	}

	private creepPool: CreepPool;

	private room: Room;

	private source: Source;

	private target: Creep | Structure;

	private extractorCreep: Creep;

	private currierCreeps: Creep[];

	constructor(
		room: Room,
		source: Source,
		target: Creep | Structure,
		creepPool: CreepPool,
		extractorCreep?: Creep,
		currierCreeps?: Creep[]) {
			if (!room || !creepPool || !source || !target) {
				log.error('ExtractEnergyOperation created without room or creepPool', room, creepPool, source, target);
			}

			this.creepPool = creepPool;
			this.room = room;
			this.source = source;
			this.target = target;
			this.currierCreeps = currierCreeps || [];

			if (extractorCreep) {
				this.extractorCreep = extractorCreep;
			}

	}

	public run() {
		let extractorCreep = this.getOrRequestExtractorCreep();
		let currierCreeps = this.getOrRequestCurrierCreeps();

		everyXTicks(() => this.constructRoad(), 100);

		if (extractorCreep) {
			Extract.execute(extractorCreep, this.source);
		}

		currierCreeps.forEach(currierCreep => {
			if (!extractorCreep) {
				return;
			}

			Currier.execute({
				creep: currierCreep,
				resourceType: RESOURCE_ENERGY,
				source: extractorCreep,
				target: this.target,
			});
		});
	}

	public serialize() {
		let currierCreeps = this.currierCreeps || [];
		let extractorCreep = this.extractorCreep || {};

		return {
			currierCreeps: currierCreeps.map(c => c.name),
			extractorCreep: extractorCreep.name,
			room: this.room.name,
			source: this.source.id,
			target: this.target.id,
			type: 'ExtractEnergy',
		};
	}

	private getOrRequestExtractorCreep(): Creep | undefined {
		if (!this.extractorCreep) {
			let creep =  this.requestExtractorCreep();
			if (creep) {
				this.extractorCreep = creep;
			}
		}

		return this.extractorCreep;
	}

	private requestExtractorCreep(): Creep | undefined {
		return this.creepPool.getCreep({
			blueprint: extractorCreepBlueprint,
			priority: 'High',
		});
	}

	private getOrRequestCurrierCreeps(): Creep[] {
		if (this.currierCreeps.length < 1) {
			let creep =  this.requestCurrierCreep();
			if (creep) {
				this.currierCreeps.push(creep);
			}
		}

		return this.currierCreeps;
	}

	private requestCurrierCreep(): Creep | undefined {
		return this.creepPool.getCreep({
			blueprint: currierCreepBlueprint,
			priority: 'High',
		});
	}

	private constructRoad(): undefined {
		if (!this.target || !this.source) {
			return;
		}

		constructRoadBetweenPoints(this.target.pos, this.source.pos);

		return;
	}
};
