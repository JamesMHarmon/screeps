import { BuildStructure } from '../task/buildStructure';
import { RepairTask } from '../task/repair.task';
import { CreepPool } from '../creepFactory/creepPool';
import { Operation } from '../operation/operation';
import { Task } from '../task/task';
import { builderCreepBlueprint } from '../blueprints/builderCreepBlueprint';
import { log } from '../components/support/log';

export class BuildAndRepairStructuresOperation implements Operation {

	public static deserialize(serializedOperation: any, creepPool: CreepPool) {
		let room = Game.rooms[serializedOperation.room];
		let energySource = Game.getObjectById<TransferableEnitity>(serializedOperation.energySource);
		let builderCreeps = (serializedOperation.builderCreeps || [])
			.map((name: string) => creepPool.claimCreep(name))
			.filter((c: Creep) => c);

		let tasks = (serializedOperation.tasks || []).filter((t: any) => {
				return ~(serializedOperation.builderCreeps || []).indexOf(t.creep);
			}).map((st: any) => RepairTask.deserialize(st));

		return new BuildAndRepairStructuresOperation(
			room,
			energySource as TransferableEnitity,
			creepPool,
			builderCreeps,
			tasks);
	}

	private creepPool: CreepPool;

	private room: Room;

	private energySource: TransferableEnitity;

	private builderCreeps: Creep[];

	private tasks: Task[];

	constructor(
		room: Room,
		energySource: TransferableEnitity,
		creepPool: CreepPool,
		builderCreeps?: Creep[],
		tasks?: RepairTask[]
	) {
			if (!room || !creepPool) {
				log.error('ExtractEnergyOperation created without room or creepPool');
			}

			this.creepPool = creepPool;
			this.room = room;
			this.energySource = energySource;
			this.builderCreeps = builderCreeps || [];
			this.tasks = tasks || [];
	}

	public run() {
		this.getOrRequestBuilderCreeps().forEach(creep => {
			let hasTask = _.any<RepairTask>(this.tasks, (task: RepairTask) => {
				return task.creep === creep;
			});

			if (hasTask) {
				return;
			}

			let damagedStructure = creep.room.find<Structure>(FIND_STRUCTURES, {
				filter: (s: Structure) => this.isRepairableNonWallStructure(s),
			})[0];

			if (damagedStructure) {
				let repairTask = new RepairTask(creep, damagedStructure, this.energySource);
				this.tasks.push(repairTask);
			} else {
				BuildStructure.execute(creep, this.energySource);
			}
		});

		this.tasks.forEach((t: RepairTask) => t.run());
	}

	public serialize() {
		let builderCreeps = this.builderCreeps;

		return {
			builderCreeps: builderCreeps.map(c => c.name),
			energySource: this.energySource.id,
			room: this.room.name,
			tasks: this.tasks
				.filter(t => !t.isComplete)
				.map(t => t.serialize()),
			type: 'BuildStructures',
		};
	}

	private getOrRequestBuilderCreeps(): Creep[] {
		if (this.builderCreeps.length < 1) {
			let creep =  this.requestBuilderCreep();
			if (creep) {
				this.builderCreeps.push(creep);
			}
		}

		return this.builderCreeps;
	}

	private requestBuilderCreep(): Creep | undefined {
		return this.creepPool.getCreep({
			blueprint: builderCreepBlueprint,
			priority: 'Low',
		});
	}

	private isRepairableNonWallStructure(structure: Structure): boolean {
		return structure.structureType !== STRUCTURE_WALL
			&& structure.structureType !== STRUCTURE_RAMPART
			&& structure.hits < structure.hitsMax * 0.70
			&& !_.any(this.tasks, (task: RepairTask) => task.target === structure);
	}
}
