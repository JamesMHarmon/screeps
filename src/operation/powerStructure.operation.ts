import { Currier } from '../task/currier';
import { CreepPool } from '../creepFactory/creepPool';
import { Operation } from '../operation/operation';
import { currierCreepSmallBlueprint } from '../blueprints/currierCreepSmallBlueprint';
import { log } from '../components/support/log';

export class PowerStructureOperation implements Operation {

	public static deserialize(serializedOperation: any, creepPool: CreepPool) {
		let room = Game.rooms[serializedOperation.room];
		let source = Game.getObjectById<TransferableEnitity>(serializedOperation.source);
		let currierCreep = creepPool.claimCreep(serializedOperation.currierCreep);

		return new PowerStructureOperation(
			room,
			source as TransferableEnitity,
			creepPool,
			currierCreep);
	}

	private creepPool: CreepPool;

	private room: Room;

	private source: TransferableEnitity;

	private currierCreep: Creep;

	constructor(
		room: Room,
		source: TransferableEnitity,
		creepPool: CreepPool,
		currierCreep?: Creep) {
			if (!room || !creepPool || !source) {
				log.error('PowerStructureOperation created without room or creepPool', room, creepPool, source);
			}

			this.creepPool = creepPool;
			this.room = room;
			this.source = source;

			if (currierCreep) {
				this.currierCreep = currierCreep;
			}
	}

	public run() {
		let currierCreep = this.getOrRequestCurrierCreep();

		if (!currierCreep) {
			return;
		}

		let target: Structure | undefined = this.getDischargedSpawn(currierCreep.pos);

		if (!target) {
			target = this.getDischargedExtension(currierCreep.pos);
		}

		if (!target) {
			target = this.getDischargedTower(currierCreep.pos);
		}

		if (target) {
			Currier.execute({
				creep: currierCreep,
				resourceType: RESOURCE_ENERGY,
				source: this.source,
				target: target,
			});
		}
	}

	public serialize() {
		let currierCreep = this.currierCreep || {};

		return {
			currierCreep: currierCreep.name,
			room: this.room.name,
			source: this.source.id,
			type: 'PowerStructure',
		};
	}

	private getOrRequestCurrierCreep(): Creep | undefined {
		if (!this.currierCreep) {
			let creep =  this.requestCurrierCreep();
			if (creep) {
				this.currierCreep = creep;
			}
		}

		return this.currierCreep;
	}

	private requestCurrierCreep(): Creep | undefined {
		return this.creepPool.getCreep({
			blueprint: currierCreepSmallBlueprint,
			priority: 'High',
		});
	}

	private getDischargedExtension(pos: RoomPosition): StructureExtension | undefined {
		return pos.findClosestByPath<StructureExtension>(FIND_MY_STRUCTURES, {
			filter: (s: Structure) => {
				if (s.structureType !== STRUCTURE_EXTENSION) {
					return false;
				}

				let structure = s as StructureExtension;

				return structure.energy < structure.energyCapacity;
			},
		});
	}

	private getDischargedSpawn(pos: RoomPosition): StructureSpawn | undefined {
		return pos.findClosestByPath<StructureSpawn>(FIND_MY_STRUCTURES, {
			filter: (s: Structure) => {
				if (s.structureType !== STRUCTURE_SPAWN) {
					return false;
				}

				let structure = s as StructureSpawn;

				return structure.energy < structure.energyCapacity;
			},
		});
	}

	private getDischargedTower(pos: RoomPosition): StructureTower | undefined {
		return pos.findClosestByPath<StructureTower>(FIND_MY_STRUCTURES, {
			filter: (s: Structure) => {
				if (s.structureType !== STRUCTURE_TOWER) {
					return false;
				}

				let structure = s as StructureTower;

				return structure.energy < structure.energyCapacity;
			},
		});
	}
};
