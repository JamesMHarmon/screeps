export function everyXTicks(run: () => any, ticks: number): undefined {
	if (Game.time % ticks === 0) {
		run();
	}

	return;
};
