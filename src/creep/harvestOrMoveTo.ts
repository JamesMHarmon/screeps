export function harvestOrMoveTo(creep: Creep, target: Source | Mineral) {
	if (creep.harvest(target) === ERR_NOT_IN_RANGE) {
		creep.moveTo(target);
	}
};
