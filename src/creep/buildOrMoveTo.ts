export function buildOrMoveTo(creep: Creep, target: ConstructionSite) {
	if (creep.build(target) === ERR_NOT_IN_RANGE) {
		creep.moveTo(target);
	}
};
