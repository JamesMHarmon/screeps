export function transferOrMoveTo(creep: Creep, target: Creep | Structure, resourceType: string) {
	let transferResult = creep.transfer(target, resourceType);

	if (transferResult === ERR_NOT_IN_RANGE) {
		creep.moveTo(target);
	}
};
