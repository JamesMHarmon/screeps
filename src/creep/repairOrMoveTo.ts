export function repairOrMoveTo(creep: Creep, target: Structure) {
	if (creep.repair(target) === ERR_NOT_IN_RANGE) {
		creep.moveTo(target);
	}
};
