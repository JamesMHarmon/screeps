export function transferFromOrMoveTo(creep: Creep, source: TransferableEnitity, resourceType: string) {
	if ((source as Creep).transfer(creep, resourceType) === ERR_NOT_IN_RANGE) {
		creep.moveTo(source);
	}
};
