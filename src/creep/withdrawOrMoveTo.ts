export function withdrawOrMoveTo(creep: Creep, target: Structure, resourceType: string) {
	if (creep.withdraw(target, resourceType) === ERR_NOT_IN_RANGE) {
		creep.moveTo(target);
	}
};
